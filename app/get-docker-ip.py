#!/usr/bin/env python
# coding: utf-8
# Save this file like get-docker-ip.py in a folder that in $PATH
# run it with
# $ docker inspect <CONTAINER ID> | get-docker-ip.py

import json
import sys

sys.stdout.write(json.load(sys.stdin)[0]['NetworkSettings']['IPAddress'])