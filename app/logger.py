#!/bin/env python
import os
import sys
import json
import csv
import datetime
import time
import psutil
import socket
from influxdb import InfluxDBClient

host_name = socket.gethostname()

# Initialize connection to the database
influx_client = InfluxDBClient('10.10.0.28', 8086, database='influx-test')
# Create database (might already exist)
# influx_client.create_database('influx-test')

while True:
    # Get load averages
    one_minute, five_minute, fifteen_minute = os.getloadavg()
    points = {'1m': one_minute, '5m': five_minute, '15m': fifteen_minute}
    # Create array of measurement points
    measurements = [
        {
            'measurement': 'load_avg',
            'tags': {
                'avg_time': key,
                'host_name': host_name,
            },
            'fields': {
                'value': float(value)
            }
        }
        for key, value in points.items()
    ]

    load_rows = [[host_name, key, float(value), datetime.datetime.now().isoformat()] for key, value in points.items()]

    with open('/app/log/load_avg.csv', 'a+') as csvfile:
        writer = csv.writer(csvfile, delimiter=',', lineterminator='\n', quoting=csv.QUOTE_ALL, dialect='excel')
        for row in load_rows:
            writer.writerow(row)

    # Get memory information
    memory = psutil.virtual_memory()
    points = {
        'total': memory.total,
        'available': memory.available,
        'percent_used': memory.percent,
    }
    # Extend the measurements array with memory information
    measurements.extend([
        {
            'measurement': 'memory',
            'tags': {
                'type': key,
                'host_name': host_name,
            },
            'fields': {
                'value': float(value)
            }
        }
        for key, value in points.items()
    ])

    memory_rows = [[host_name, key, float(value), datetime.datetime.now().isoformat()]
                   for key, value in points.items()]

    with open('/app/log/memory_log.csv', 'a+') as csvfile:
        writer = csv.writer(csvfile, delimiter=',', lineterminator='\n', quoting=csv.QUOTE_ALL, dialect='excel')
        for row in memory_rows:
            writer.writerow(row)

    # Get /data partition information
    disk_usage = psutil.disk_usage('/')
    points = {
        'total': disk_usage.total,
        'used': disk_usage.used,
        'free': disk_usage.free,
    }
    # Extend the measurements array with memory information
    measurements.extend([
        {
            'measurement': 'disk_usage',
            'tags': {
                'type': key,
                'host_name': host_name,
            },
            'fields': {
                'value': float(value)
            }
        }
        for key, value in points.items()
    ])

    disk_rows = [[host_name, key, float(value), datetime.datetime.now().isoformat()]
                 for key, value in points.items()]

    with open('/app/log/disk_usage.csv', 'a+') as csvfile:
        writer = csv.writer(csvfile, delimiter=',', lineterminator='\n', quoting=csv.QUOTE_ALL, dialect='excel')
        for row in disk_rows:
            writer.writerow(row)

    # Write load averages to the database
    influx_client.write_points(measurements)
    time.sleep(1)
